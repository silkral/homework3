import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;
import java.util.StringTokenizer;
import javax.print.attribute.standard.RequestingUserName;

//Kasutatud materjalid: http://www.mycstutorials.com/articles/data_structures/stacks; 
//http://stackoverflow.com/questions/14100522/reverse-polish-notation-java

public class DoubleStack {

	private LinkedList<Double> stack;
	private final static double epsilon = 0.00000001;
	private static double d;

	public static void main(String[] argum) {
		// TODO!!! Your tests here!
//		DoubleStack a = new DoubleStack();
//		DoubleStack c = new DoubleStack();
//		c.push(1);
//		System.out.println(c);
//		c.pop();
//		System.out.println(c.stEmpty());
//		System.out.println(c.equals(a));
//		System.out.println(a);
//		a.push(2.5);
//		System.out.println(a);
//		a.push(3.5);
//		a.push(7.7);
//		a.push(2.3);
//		System.out.println(a);
//		double tulemus = a.pop();
//		System.out.println(a);
//		DoubleStack koopia = a;
//		DoubleStack b = new DoubleStack();
//		b.push(2.5);
//		b.push(3.5);
//		b.push(7.7);
//		b.push(2.3);
//		System.out.println(b.equals(a));
//		System.out.println(a);
//		System.out.println(a.tos());
//		System.out.println(a);
//
//		System.out.println(b);
//
//		// koopia = (DoubleStack)a.clone();
//		System.out.println(koopia.equals(a));
//		System.out.println(a);
//		System.out.println(koopia);
//
//		a.push(9.9);
//		koopia.push(1.1);
//		// System.out.println(a);
//		// System.out.println(koopia);
//		a.pop();
//		// System.out.println(koopia.equals(a));
//		System.out.println(a);
//		// System.out.println(koopia);
//		// System.out.println(a.tos());
//
		String pol = "2. 5. - +";
		double r = interpret(pol);
		System.out.println(r);

	}

	private int size() {// stack size
		return stack.size();
	}

	private double peek(int i) {// stack peek
		return (double) stack.get(i);
	}

	
	public static boolean isNumeric(String str) {
		try {
			d = Double.parseDouble(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	DoubleStack() {// uue magasini konstruktor

		this.stack = new LinkedList<Double>();

	}

	@Override
	public Object clone() throws CloneNotSupportedException { // koopia loomine

		DoubleStack stack2 = new DoubleStack();

		stack2.stack = (LinkedList<Double>) stack.clone();
		return stack2;
	}

	public boolean stEmpty() { // magasini tühjuse kontroll - kui tühi, siis
								// true

		return stack.isEmpty();

	}// stEmpty

	public void push(double a) {// magasini elemendi lisamine:

		stack.addLast(a);

	}// push

	public double pop() {// magasinist elemendi võtmine

		if (stEmpty())
			throw new RuntimeException("Stack underflow");// if no items,
															// throw exception
		return stack.removeLast();

	} // pop

	public void op(String s) { // aritmeetikatehe s ( <code>+ - * /</code> )
								// magasini kahe pealmise elemendi
								// vahel (tulemus pannakse uueks tipuks):

		if (stEmpty())
			throw new RuntimeException("Stack underflow");// if no items,
															// throw exception
		if (stack.size() < 2) {// kontrollib operandide arvu
			throw new RuntimeException("Pole tehte tegemiseks piisavalt arve");

		} // kas on vähemalt 2 operandi võtta

		if (!s.equals("+") && !s.equals("-") && !s.equals("*") && !s.equals("/")) {// kui
			// element
			// ei ole
			// meile
			// sobiv
			// tehtemärk
			throw new RuntimeException("Kontrolli tehtemärki, see ei ole +-/*");}

		
		double op2 = (double) pop();
		double op1 = (double) pop();
		if (s.equals("+"))
			push(op1 + op2);
		if (s.equals("-"))
			push(op1 - op2);
		if (s.equals("*"))
			push(op1 * op2);
		if (s.equals("/"))
			push(op1 / op2);
	}// op

	public double tos() { // tipu lugemine eemaldamiseta:
		if (stack.isEmpty()) {
			throw new RuntimeException("Magasin on tühi");
		}
		return stack.getLast();

	}// tos

	@Override
	public boolean equals(Object o) {// kahe magasini võrdsuse kindlakstegemine:

		DoubleStack other = (DoubleStack) o;// object cast to DoubleStack

		// if (stack == o) //// If they point to the same object return true
		// return true;

		// if (stack.isEmpty() && other.stack.isEmpty())//if both empty
		// return true;

		if (this.size() != other.size()) {
			return false;
		}

		for (int i = 0; i < stack.size(); i++) {

			// if (Math.abs(stack.get(i)- other.stack.get(i))>epsilon){
			if (Math.abs(this.peek(i) - other.peek(i)) > epsilon) {

				return false;
			} // if
		} // for

		return true;
	}// equals

	@Override
	public String toString() { // teisendus sõneks (tipp lõpus):

		if (stack.isEmpty())
			return "Tühi";

		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < stack.size(); i++) {
			sb.append(stack.get(i));
			sb.append(" ");
		}
		return sb.toString();

	}// toString

	public static double interpret(String pol) {

		pol = pol.trim();// eemaldab mittevajalikud tühikud??

		int pikkus = pol.length(); // test empty
		if (pikkus == 0) {
			throw new RuntimeException("String empty");
		} // if no items

		DoubleStack uusStack = new DoubleStack();
		StringTokenizer st = new StringTokenizer(pol);// juppideks
		String elem;
		
		while (st.hasMoreElements()) {// sisu kontroll
			elem = st.nextToken();// üksik element stringina
			if (isNumeric(elem)) {// kui element on arv, siis lisa stacki
				uusStack.push(d);
			} // if1 - kui on number

			if (elem.equals("+") || elem.equals("-") || elem.equals("*") || elem.equals("/")) {// kui
																								// element
																								// on
																								// meile
																								// sobiv
																								// tehtemärk
				if (uusStack.size() < 2) {// kontrollib operandide arvu
					throw new RuntimeException("Sisestatud avaldises  ("+ pol + "  ) pole tehte tegemiseks piisavalt arve");

				} // if3-kas on vähemalt 2 operandi võtta

				uusStack.op(elem);// teeb tehte
			} // if2 - kui on meile sobiv tehtemärk

		} // while

		if (uusStack.size() > 1)
			throw new RuntimeException(
					"Teie avaldises  (" + "pol" + "  ) oli liiga palju arve või tehtemärk esines liiga vara");

		return uusStack.tos();

		// return 0.;
		// TODO!!! Your code here! Koostada meetod
		// aritmeetilise avaldise pööratud poola kuju (sulgudeta postfikskuju,
		// Reverse Polish Notation) <code>pol</code> interpreteerimiseks
		// (väljaarvutamiseks)
		// eelpool defineeritud reaalarvude magasini abil. Avaldis on antud
		// stringina,
		// mis võib sisaldada reaalarve (s.h. negatiivseid ja mitmekohalisi)
		// ning
		// tehtemärke <code> + - * / </code>, mis on eraldatud tühikutega
		// (whitespace).
		// Tulemuseks peab olema avaldise väärtus reaalarvuna või erindi
		// (RuntimeException)
		// tekitamine, kui avaldis ei ole korrektne. Korrektne ei ole, kui
		// avaldises
		// esineb lubamatuid sümboleid, kui avaldis jätab magasini üleliigseid
		// elemente
		// või kasutab magasinist liiga palju elemente.
		// <code>DoubleStack.interpret ("2. 15. -")</code> peaks tagastama
		// väärtuse
		// <code> -13.</code> .
		//
		// kui esineb arv, siis sisesta
		// kui tehe, siis korjatakse esmalt teine parameeter ja siis teine
		// parameeter ja tehsksse tehe - alumine - ülemine;
		// arvutatakse summa ja tulemus pannakse magasini
		// veaolukorrad:
		// 1. tundmat märgikombinatsioon - runtime exception ja öelda tuleb et
		// mis oli string mida parsiti ja mis oli viga näite tundmatu sümbol oli
		// xxx
		// terve vigane avaldis tuleb tagasi peegeldada
		// 2. alatäitumine - mingi tehtemärk on liiga vara, sel juhul tuleb ise
		// kinni püüda; kogu string veateatesse //op ei näe suurt konteksti
		// 3. kui arve on liiga palju e kui lõpus jääb rohkem kui üks element.
		// lõpus peab jääma üks. kui on rohkem kui üks, siis on viga
		// näiteks esitada esialgne sisend ja öelda, et siin on liiga palju arve
		// teie avaldises oli xxx..." "" tehtemärk esines liiga vara"
		// siin tuleks teha s kontroll, et s poleks midagi jama; equals peab
		// olema minu tehtud
		// teha tokeniteks . stringtokenizeriga sisendvoog jagada; kõiki white
		// space käsitleda tühikuna. võib olla tabulaatorina sees

	}

}
